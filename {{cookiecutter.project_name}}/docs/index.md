# {{ cookiecutter.project_name }}

For documentation see tests: [{{ cookiecutter.repository_url }}/-/tree/main/tests]({{ cookiecutter.repository_url }}/-/tree/main/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [{{ cookiecutter.bug_tracker_url }}]({{ cookiecutter.bug_tracker_url }})

## Requirements

Install requirements for setup beforehand using

```bash
poetry install -E test
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license. See [LICENSE]({{ cookiecutter.repository_url }}/-/blob/main/LICENSE) for more information.

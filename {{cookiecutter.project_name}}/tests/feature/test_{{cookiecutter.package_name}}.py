from {{cookiecutter.package_name}} import {{cookiecutter.package_name}}

def test_{{cookiecutter.package_name}}():
  assert {{cookiecutter.package_name}}() == '{{cookiecutter.package_name}}'

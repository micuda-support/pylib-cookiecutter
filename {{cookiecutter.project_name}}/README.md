# {{ cookiecutter.project_name }}
[![pipeline status]({{ cookiecutter.repository_url }}/badges/main/pipeline.svg)]({{ cookiecutter.repository_url }}/-/commits/main)
[![coverage report]({{ cookiecutter.repository_url }}/badges/main/coverage.svg)]({{ cookiecutter.repository_url }}/-/commits/main)

**This is the readme for developers.** The documentation for users is available here: [{{ cookiecutter.documentation_url }}]({{ cookiecutter.documentation_url }})

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [{{ cookiecutter.bug_tracker_url }}]({{ cookiecutter.bug_tracker_url }})

## Requirements for builds

Install requirements for setup beforehand using

```bash
poetry install -E test -E build
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license. See [LICENSE]({{ cookiecutter.repository_url }}/-/blob/main/LICENSE) for more information.

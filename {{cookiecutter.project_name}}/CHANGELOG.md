# Changelog

Tracking changes in `{{ cookiecutter.project_name }}` between versions. For a complete view of all the releases, visit GitLab:

{{ cookiecutter.repository_url }}/-/releases

## next release

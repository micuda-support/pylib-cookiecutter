# pylib-cookiecutter

## Features

* includes license file (MIT)
* documentation powered by [`mkdocs`](https://www.mkdocs.org/) with versioning support via [`mike`](https://github.com/jimporter/mike)
* [`pytest`](https://pytest.org/) for testing
* packaging and dependency management powered by [`poetry`](https://python-poetry.org/)
* preconfigured GitLab CI/CD
  * automatic testing
  * automatic generating and publishing documentation with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
  * automatic building of package and publishing it to [PYPI](https://pypi.org/)
* badges for pipeline status, coverage

## Usage and options

First generate your project:

```bash
  cookiecutter gl:micuda-support/pylib-cookiecutter
```

You will be asked for these options:

 option              | default                                                            | description                              | used in
 ------------------- | ------------------------------------------------------------------ | ---------------------------------------- | ----------------------------------------------------------------------------
 author_full_name    | Adam Mičuda                                                        | main author of this library              | `pyproject.toml`, `LICENSE`
 author_email        | adam<area>@micuda.dev                                                    | contact email of the author              | `pyproject.toml`
 project_name        | python-library                                                     | name of the GitLab project               | `pyproject.toml`, `mkdocs.yml`, `README.md`, `CHANGELOG.md`, `docs/index.md`
 project_group       | python-toolbelt                                                    | name of the GitLab group                 | it is part of the default values of URLs
 package_name        | python_library                                                     | name of the python package               | `pyproject.toml`, the default test
 description         |                                                                    | description of the project               | `pyproject.toml`
 keywords            |                                                                    | keywords                                 | `pyproject.toml`
 initial_version     | 0.1.0-alpha1                                                       | initial version of the project           | `pyproject.toml`
 base_python_version | 3.8.6                                                              | minimal version of Python                | `pyproject.toml`, `.python-version`
 repository_url      | http<area>s://gitlab.com/python-toolbelt/python-library            | url of the repository                    | `pyproject.toml`, `README.md`, `docs/index.md`
 homepage_url        | http<area>s://python-toolbelt.gitlab.io/python-library             | url of the project homepage              | `pyproject.toml`
 documentation_url   | http<area>s://python-toolbelt.gitlab.io/python-library             | url of the documentation                 | `pyproject.toml`, `README.md`
 bug_tracker_url     | http<area>s://gitlab.com/python-toolbelt/python-library/-/issues   | url of the bug tracker                   | `pyproject.toml`, `README.md`, `docs/index.md`
 copyright_applied   | [current_year]                                                     | date when copyright was applied          | `LICENSE`

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/micuda-support/pylib-cookiecutter/-/blob/main/LICENSE) for more information.

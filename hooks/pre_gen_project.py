"""
{{ cookiecutter.update({ 'author':    cookiecutter.author_full_name + ' <' + cookiecutter.author_email + '>'}) }}
{{ cookiecutter.update({ 'keywords':  cookiecutter.keywords.split(',') | map('trim') | reject('eq', '') | list | string | replace("'", '"') })  }}
"""
